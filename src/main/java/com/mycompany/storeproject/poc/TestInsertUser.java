/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.User;



/**
 *
 * @author admin02
 */
public class TestInsertUser {
  public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "INSERT INTO user (name,tel,password)VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            User user = new User(-1,"Baekhyun","0955555555","home123");
            stmt.setString(1,user.getName());
            stmt.setString(2,user.getTel());
            stmt.setString(3,user.getPassword());
            int row =stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if(result.next()){
                id = result.getInt(1);
            }
             System.out.println("Affect row" + row+"id:"+id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }

}

